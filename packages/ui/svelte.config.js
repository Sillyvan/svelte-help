import preprocess from 'svelte-preprocess';

const config = {
  package: {
    source: 'src/lib',
  },
  // Consult https://kit.svelte.dev/docs/integrations#preprocessors
  // for more information about preprocessors
  preprocess: [
    preprocess({
      postcss: true,
    }),
  ],
};

export default config;
